# Local IspellDict: en
#+SPDX-FileCopyrightText: 2019-2024 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+STARTUP: showeverything
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="./reveal.js/dist/theme/index.css" />
#+TITLE: Open Educational Resource (OER) presentations as part of a module on Communication and Collaboration Systems
#+AUTHOR: Jens Lechtenbörger
#+OPTIONS: html-style:nil
#+OPTIONS: toc:nil
#+KEYWORDS: OER, distributed system, internet, e-mail, web, git
#+DESCRIPTION: Introduction with open educational resources (OER) to distributed systems from the perspective of communication and collaboration systems.  Starting from major notions of distributed systems, the Internet is presented as communication backbone, based on which applications such as the Web, E-Mail, and Git are built

* What is this?

**Historical project.**  This course was taught for the last time in 2023.

#+INCLUDE: "README.org"

** Distributed Systems with Git on GitLab
If you are teaching or learning Distributed Systems, feel free to use,
share, and adapt my OER.  As usual for projects on GitLab,
you can open
[[https://gitlab.com/oer/oer-courses/cacs/-/issues][issues]]
([[https://docs.gitlab.com/ee/user/project/issues/managing_issues.html][GitLab documentation on issues]])
to report bugs or suggest improvements, ideally
with [[https://gitlab.com/oer/oer-courses/cacs/-/merge_requests][merge requests]]
([[https://docs.gitlab.com/ee/user/project/merge_requests/][GitLab documentation on merge requests]]).
Note that a video below contains a step-by-step description of the
creation of a merge request on GitLab.

Presentations make use of the HTML presentation framework
[[https://revealjs.com/][reveal.js]].

* Presentations and other OER
  :PROPERTIES:
  :CUSTOM_ID: presentations
  :END:
# *WARNING!* Resources below will be updated during the term.
# They are “ready” if a link in Learnweb exists.

- Git
  - General comments on Git and examples for collaboration with Git on
    GitHub and GitLab: [[./videos/Git-Examples.mp4][Video]],
    [[file:texts/Git-Examples.org][script of video as HTML page]] ([[file:texts/Git-Examples.pdf][PDF]])
  - [[file:Git-Introduction.org][*Git Introduction*]] as presentation
    ([[file:pdfs/Git%20Introduction.pdf][PDF with screen layout]],
    [[file:Git-Introduction.pdf][condensed PDF]])
  - [[file:texts/GitLab-Quickstart.org][Quickstart for GitLab with SSH keys]] ([[file:texts/GitLab-Quickstart.pdf][PDF]])
  - [[file:texts/Git-Workflow-Instructions.org][Instructions for group exercise on Feature Branch Workflow]] ([[file:texts/Git-Workflow-Instructions.pdf][PDF]])
- [[file:Distributed-Systems-Introduction.org][*Introduction to Distributed Systems*]]
  ([[file:pdfs/Distributed%20Systems.pdf][PDF with screen layout]],
  [[file:Distributed-Systems-Introduction.pdf][condensed PDF]])
- [[file:Internet.org][*Introduction to the Internet*]]
  ([[file:pdfs/The%20Internet.pdf][PDF with screen layout]],
  [[file:Internet.pdf][condensed PDF]])
  - [[file:Wireshark-Demo.org][Wireshark Demo]]
- [[file:Web-and-E-Mail.org][*Web and E-Mail*]]
  ([[file:pdfs/Web%20and%20E-Mail.pdf][PDF with screen layout]],
  [[file:Web-and-E-Mail.pdf][condensed PDF]])

* Source code and licenses
#+MACRO: gitlablink [[https://gitlab.com/oer/oer-courses/cacs][$1]]
#+INCLUDE: "~/.emacs.d/oer-reveal-org/source-code-licenses.org"
