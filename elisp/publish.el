;;; publish.el --- Publish reveal.js presentation, HTML, and PDF from Org sources
;; -*- Mode: Emacs-Lisp -*-
;; -*- coding: utf-8 -*-

;; SPDX-FileCopyrightText: 2017-2022 Jens Lechtenbörger
;; SPDX-License-Identifier: GPL-3.0-or-later

;;; License: GPLv3

;;; Commentary:
;; Mostly, settings from emacs-reveal-publish are used.
;; Besides, non-free logos are published here.
;;
;; Use this file from its parent directory with the following shell
;; command:
;; emacs --batch --load elisp/publish.el

;;; Code:
;; Avoid update of emacs-reveal, enable stacktraces.
(setq emacs-reveal-managed-install-p nil
      debug-on-error t)

;; Set up load-path.
(let ((install-dir
       (mapconcat #'file-name-as-directory
                  `(,user-emacs-directory "elpa" "emacs-reveal") "")))
  (add-to-list 'load-path install-dir)
  (condition-case nil
      ;; Either require package with above hard-coded location
      ;; (e.g., in docker) ...
      (require 'emacs-reveal)
    (error
     ;; ... or look for sibling "emacs-reveal" of oer-courses.
     (add-to-list
      'load-path
      (expand-file-name "../../../emacs-reveal/" (file-name-directory load-file-name)))
     (require 'emacs-reveal))))

;; Dirty fix to avoid @@latex... showing up in HTML title tag.
(setq oer-reveal-alternate-type-latex "\n\n@@latex:Note: %s@@\n")

;; ;; Generate multiplex client presentations if name contains "multiplex".
;; (setq org-re-reveal-client-multiplex-filter "multiplex")
;; (add-to-list 'oer-reveal-publish-org-publishing-functions
;;              #'oer-reveal-publish-to-reveal-client)

(oer-reveal-publish-all
 (list
  (list "texts"
       	:base-directory "texts"
       	:base-extension "org"
	:exclude "config\\|license-template"
        :html-postamble ""
       	:publishing-function '(oer-reveal-publish-to-html-and-pdf)
       	:publishing-directory "./public/texts")
  (list "qr-codes"
	:base-directory "qr-codes"
	:base-extension "png"
	:publishing-directory "./public/qr-codes"
	:publishing-function 'org-publish-attachment)
  (list "videos"
	:base-directory "videos"
	:base-extension "mp4"
	:publishing-directory "./public/videos"
	:publishing-function 'org-publish-attachment)
  (list "socket.io"
        :base-directory "js/socket.io"
        :base-extension "js"
        :publishing-directory "./public/reveal.js/plugin/multiplex"
        :publishing-function 'org-publish-attachment)
  (list "prog-imgs"
        :base-directory "programming/texts"
        :base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
        :publishing-function 'org-publish-attachment
        :publishing-directory "./public/programming/texts")
  (list "prog-audio"
	:base-directory "programming/audio"
	:base-extension (regexp-opt '("ogg" "mp3"))
	:publishing-directory "./public/audio"
	:publishing-function 'org-publish-attachment)
  (list "prog-quizzes"
	:base-directory "programming/quizzes"
	:base-extension (regexp-opt '("js"))
	:publishing-directory "./public/quizzes"
	:publishing-function 'org-publish-attachment)
  (list "ds-audio"
	:base-directory "distributed-systems/audio"
	:base-extension (regexp-opt '("ogg" "mp3"))
	:publishing-directory "./public/audio"
	:publishing-function 'org-publish-attachment)
  (list "ds-quizzes"
	:base-directory "distributed-systems/quizzes"
	:base-extension (regexp-opt '("js"))
	:publishing-directory "./public/quizzes"
	:publishing-function 'org-publish-attachment)
  (list "title-logos"
	:base-directory "non-free-logos/title-slide"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/title-slide"
	:publishing-function 'org-publish-attachment)
  (list "theme-logos"
	:base-directory "non-free-logos/reveal-css"
	:base-extension (regexp-opt '("png" "jpg" "ico" "svg" "gif"))
	:publishing-directory "./public/reveal.js/css/theme"
	:publishing-function 'org-publish-attachment)))

(provide 'publish)
;;; publish.el ends here
